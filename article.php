<?
/**
 * Класс статьи, просто чтобы исспользовать типы... Можно не вникать
 */
class Article {
    
    /**
     * @var integer Идентификатор статьи
     */
    public $id;

    /**
     * @var integer Время создания статьи (timestamp)
     */
    public $time;

    /**
     * @var string Заголовок статьи
     */
    public $title;

    /**
     * @var string Текст статьи
     */
    public $text;
}
